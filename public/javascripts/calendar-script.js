var selectedUser;
var calendarDay;
var activeDate;

// Koerer naar '/' route bliver kaldt.
function initializeCalendar() {
    activeDate = moment();
    generateCalendarItem();
    updateView();
}

function updateView() {
    displayDate();
    generateCalDays();
    generateTimeLine();
    showAbsence();
}

// event handler for next btn
$('.btn-next').on('click', function (event) {

    var active_view = $('.calendar-nav-right').find('.active');

    if (active_view.hasClass('display-week')) {
        activeDate.add(1, 'day');
    } else if (active_view.hasClass('display-month')) {
        activeDate.add(1, 'week');
    } else if (active_view.hasClass('display-quarter')) {
        activeDate.add(1, 'month');
    } else if (active_view.hasClass('display-year')) {
        activeDate.add(1, 'year');
    }

    updateView();
});

// event handler for prev btn
$('.btn-prev').on('click', function (event) {
    var active_view = $('.calendar-nav-right').find('.active');

    if (active_view.hasClass('display-week')) {
        activeDate.subtract(1, 'day');
    } else if (active_view.hasClass('display-month')) {
        activeDate.subtract(1, 'week');
    } else if (active_view.hasClass('display-quarter')) {
        activeDate.subtract(1, 'month');
    } else if (active_view.hasClass('display-year')) {
        activeDate.subtract(1, 'year');
    }

    updateView();
});

$('.btn-today').on('click', function (event) {
    activeDate = moment();
    updateView();
});

$('.calendar-timeLine').on('mouseenter', '.timeLine-day', function( event ) {
    var index = $('.timeLine-day').index(this);

    $('.timeLine-day').eq(index).css({'background-color': 'rgba(16, 16, 16, 0.4)'});

    $('.calendar-item').each(function (i, elem) {
        $(elem).find('.day-container').eq(index).css({'background-color': 'rgba(16, 16, 16, 0.4)'});
    });
}).on('mouseleave', '.timeLine-day', function( event ) {
    var index = $('.timeLine-day').index(this);

    var colors = ['rgba(169, 169, 169, 0.2)', 'rgba(211, 211, 211, 0.2)'];

    $('.timeLine-day').eq(index).css({'background-color': colors[index%2]});

    $('.calendar-item').each(function (i, elem) {
        $(elem).find('.day-container').eq(index).css({'background-color': 'inherit'});
    });

});

$('.calendar-content').on('mouseenter', '.calendar-day', function( event ) {

    if ($(this).attr('data-absence') != undefined) {
        var absenceID = $(this).attr('data-absence');
        var all_divs = $('body').find("[data-absence='" + absenceID +"']");

        $(all_divs).each(function (index, item) {

            if ($(item).hasClass('start-abs')) {
                $(item).css({'border-left': '3px solid black'});
                $(item).css({'border-top': '3px solid black'});
                $(item).css({'border-bottom': '3px solid black'});
            }

            if ($(item).hasClass('end-abs')) {
                $(item).css({'border-right': '3px solid black'});
                $(item).css({'border-top': '3px solid black'});
                $(item).css({'border-bottom': '3px solid black'});
            }

            if (!$(item).hasClass('end-abs') && !$(item).hasClass('end-abs')) {
                $(item).css({'border-top': '3px solid black'});
                $(item).css({'border-bottom': '3px solid black'});
            }
        });

    }

}).on('mouseleave', '.calendar-day', function( event ) {
    if ($(this).attr('data-absence') != undefined) {

        var absenceID = $(this).attr('data-absence');
        var all_divs = $('body').find("[data-absence='" + absenceID +"']");

        $(all_divs).each(function (index, item) {
            $(item).css({'border': 'none'});
        });


    }
});

$('.calendar-nav-right').on('click', '.btn', function (event) {

    var pressed_btn = $(this);
    var all_btns = $(pressed_btn).siblings('.btn');

    $(all_btns).each(function (index, btn) {
        if ($(btn).hasClass('active')) {
            $(btn).removeClass('active');
        }
    });

    pressed_btn.addClass('active');

    updateView();
});

$('.calendar-timeLine').on('click', '.timeLine-day', function () {

    if ($(this).hasClass('week')) {
        activeDate = moment(+$(this).attr('data-date'));
        $('.display-week').trigger('click');
    } else if($(this).hasClass('month')) {
        activeDate = moment(+$(this).attr('data-date'));
        $('.display-month').trigger('click');
    } else if($(this).hasClass('year')) {
        activeDate = moment(+$(this).attr('data-date'));
        $('.display-month').trigger('click');
    }

});

function generateCalDays() {

    var date = moment(activeDate);
    var active_view = $('.calendar-nav-right').find('.active');
    var daysInAWeek = 7;

    $('.calendar-item').empty();

    if (active_view.hasClass('display-week')) {

        for (var i = 1 ; i <= daysInAWeek ; i++) {
            $('.calendar-item').append($('<div class="day-container"><div class="calendar-day" data-date="'+new Date(date).toDateString()+'">'+
                '</div></div>'));
            date.add(1, 'day');
        }

    } else if (active_view.hasClass('display-month')) {

        date.day(1);

        for (let i = 0 ; i < (daysInAWeek * 4) ; i++) {

            if (i % daysInAWeek == 0) {
                $('.calendar-item').append($('<div class="day-container"></div>'));
            }

            $('.calendar-item').each(function (index, item) {
                $(item).find('.day-container').last().append($('<div class="calendar-day" data-date="'+new Date(date).toDateString()+'">'+
                    '</div>'));
            });

            date.add(1, 'day');
        }

    } else if (active_view.hasClass('display-quarter')) {

        date.startOf('month');
        var lastMonth = moment(date).add(3, 'month');
        var currMonth = undefined;

        while (date.month() != lastMonth.month()) {

            if (currMonth != date.month()) {
                currMonth = date.month();
                $('.calendar-item').append($('<div class="day-container"></div>'));
            }

            $('.calendar-item').each(function (index, item) {

                $(item).find('.day-container').last().append($('<div class="calendar-day" data-date="'+new Date(date).toDateString()+'">'+
                    '</div>'))
            });

            date.add(1, 'day');

        }

    } else if (active_view.hasClass('display-year')) {

        date.startOf('year');
        var lastMonth = moment(date).add(1, 'year');
        var currMonth = undefined;

        while (date.year() != lastMonth.year()) {

            if (currMonth != date.month()) {
                currMonth = date.month();
                $('.calendar-item').append($('<div class="day-container"></div>'));
            }

            $('.calendar-item').each(function (index, item) {

                $(item).find('.day-container').last().append($('<div class="calendar-day" data-date="'+new Date(date).toDateString()+'">'+
                    '</div>'))
            });

            date.add(1, 'day');
        }

    }

}

function displayDate() {

    var active_btn = $('.calendar-nav-right > .active');

    $('.nav-ext').text("");

    if ($(active_btn).hasClass('display-week')) {
        var lastDate = moment(activeDate).add(6, 'days');
        var str = activeDate.format('[Uge] w ');

        if (+activeDate.format('w') < +lastDate.format('w')) {
            str += lastDate.format('[-] w');
        }

        str += activeDate.format(' MMMM YYYY');

        $('.nav-ext').text(str);
    } else if ($(active_btn).hasClass('display-month')) {

        var lastDate = moment(activeDate).add(27, 'days');
        var str = activeDate.format('MMMM ');

        if (+activeDate.format('M') < +lastDate.format('M')) {
            str += lastDate.format('[-] MMMM');
        }

        str += activeDate.format(' YYYY');

        $('.nav-ext').text(str);

    } else if ($(active_btn).hasClass('display-quarter') || $(active_btn).hasClass('display-year')) {
        $('.nav-ext').text(activeDate.format('YYYY'));
    }

}

function generateTimeLine() {

    $('.calendar-timeLine').empty();

    var qty = $('.calendar-nav-right > .active').attr('data-days');
    var active_btn = $('.calendar-nav-right > .active');
    var date = moment(activeDate);

    if ($(active_btn).hasClass('display-week')) {
        for (var i = 1 ; i <= qty ; i++) {
            $('.calendar-timeLine').append($('<div class="timeLine-day day">'+
                date.format('ddd Do')  +
                '</div>'));
            date.add(1, 'day');
        }
    } else if ($(active_btn).hasClass('display-month')) {

        date.day(1);

        for (var i = 0 ; i < 4 ; i++) {
            $('.calendar-timeLine').append($('<div class="timeLine-day week" data-date="'+date+'">'+
                'Uge ' + date.format('w')  +
                '</div>'));
            date.add(1, "week");
        }

    } else if ($(active_btn).hasClass('display-quarter')) {

        date.startOf('month');

        for (var i = 0 ; i < 3 ; i++) {
            $('.calendar-timeLine').append($('<div class="timeLine-day month" data-date="'+date+'">'+
                date.format('MMMM')  +
                '</div>'));
            date.add(1, 'month');
        }

    } else if ($(active_btn).hasClass('display-year')) {

        date.startOf('year');

        for (var i = 0 ; i < 12 ; i++) {
            $('.calendar-timeLine').append($('<div class="timeLine-day year" data-date="'+date+'">'+
                date.format('MMM')  +
                '</div>'));
            date.add(1, 'month');
        }

    }

}

function generateCalendarItem() {

    for (var i = 0 ; i < $('.user-item').length ; i++) {
        $('.calendar-content').append($('<div class="calendar-item" data-user="' +
            $('.user-item').eq(i).attr('data-id') +'"></div>'));
    }

}

function showAbsence() {

    //TODO: det er da dumt!
    var from = $($('.calendar-item').first().find(".calendar-day").first()).attr('data-date');
    var to = $($('.calendar-item').first().find(".calendar-day").last()).attr('data-date');

    // Seconds istedet for milliseconds
    from = new Date(moment(new Date(from)).startOf('day')).getTime() / 1000;
    to = new Date(moment(new Date(to)).endOf('day')).getTime() / 1000;

    $.get('/api/absence/'+from+'/'+to+'')

        .done(function (absences) {
            $('.calendar-item').each(function (index, item) {
                var userId = $(item).attr('data-user');

                var userAbsence = absences.filter(function (absence) {
                    return absence.user == userId;
                });

                $($(item).find('.calendar-day')).each(function (index, item) {
                    var date = $(item).attr('data-date');

                    for (var absence of userAbsence) {

                        var now = moment(new Date(date));
                        var start = moment(absence.startDate).startOf('day');
                        var end = moment(absence.endDate);

                        if (now.isBetween(start, end) || now.isSame(start)) {
                            $(item).addClass(absence.status);
                            $(item).attr('data-absence', absence._id);

                            if (now.isSame(start)) {
                                $(item).addClass('start-abs');
                            }
                            end.startOf('day');
                            if (now.isSame(end)) {
                                $(item).addClass('end-abs');
                            }

                        }

                    }

                });

            });
        })
        .fail(function (err) {
            console.log(err.responseText);
        });
}

$(document).ready(function() {
    $(".calendar-content").delegate(".calendar-day", "click", function (event) {

        $("span").css("pointer-events", "auto");

        calendarDay = $(this);
        selectedUser = $(calendarDay).closest(".calendar-item").attr("data-user");

        var absenceId = $(calendarDay).attr('data-absence');

        if (absenceId == undefined) {
            if (selectedUser == $(".calendar-content").attr("data-id")) {
                $('#startDatePicker').datepicker('update', new Date($(event.target).attr("data-date")));
                $('#endDatePicker').datepicker('update', new Date($(event.target).attr("data-date")));
                $("#absenceComment").val("");
                $("#chbPrivate").prop('checked', value = 0);
                $('#createAbsenceModal').modal('toggle');

                checkAbsenceValidity();
            }
        } else if (absenceId != undefined) {
            $.get('/api/absence/'+absenceId+'')
                .done(function (absence) {
                    if (absence.user == $(".calendar-content").attr("data-id")) {
                        $.get('/api/users/'+absence.user+'')
                            .done(function (user) {
                                if ($(user).first().attr("approver") == $(".calendar-content").attr("data-id") || absence.status == "Pending") {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset() * 60 * 1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset() * 60 * 1000);

                                    $('#startDateUpdater').datepicker('update', startDate);
                                    $('#endDateUpdater').datepicker('update', endDate);
                                    $("#updateAbsenceCommentApprover").val(absence.comment);
                                    $("#updateChbPrivateApprover").prop('checked', value = absence.privateComment);
                                    $('#updateAbsenceModal').modal('toggle');

                                    checkAbsenceValidity();

                                } else {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset() * 60 * 1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset() * 60 * 1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $("#updateAbsenceCommentUser").val(absence.comment);
                                    $("#updateChbPrivateUser").prop('checked', value = absence.privateComment);
                                    $('#updateApprovedAbsenceModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);
                                }
                            });
                    } else if (absence.privateComment) {
                        $.get('/api/users/'+absence.user+'')
                            .done(function (user) {
                                if ($(user).first().attr("approver") != $(".calendar-content").attr("data-id")) {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset() * 60 * 1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset() * 60 * 1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('#viewRequestPrivateModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);

                                } else if (absence.status == "Approved") {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset() * 60 * 1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset() * 60 * 1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('.viewAbsenceComment').val(absence.comment);
                                    $('#viewChbPrivate').prop('checked', value = absence.privateComment);
                                    $('#viewApprovedAbsenceModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);
                                    $(".viewAbsenceComment").prop('disabled', true);
                                } else {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset() * 60 * 1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset() * 60 * 1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('.viewAbsenceComment').val(absence.comment);
                                    $('#viewChbPrivate').prop('checked', value = absence.privateComment);
                                    $('#approverRequestModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);
                                    $(".viewAbsenceComment").prop('disabled', true);
                                }
                            });
                    } else {
                        $.get('/api/users/'+absence.user+'')
                            .done(function (user) {
                                if ($(user).first().attr("approver") != $(".calendar-content").attr("data-id")) {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset()*60*1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset()*60*1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('.viewAbsenceComment').val(absence.comment);
                                    $('#viewRequestPublicModal').modal('toggle');


                                    $("span").css("pointer-events", "none");
                                    $(".startDateViewer :input").prop("disabled", true);
                                    $(".endDateViewer :input").prop("disabled", true);
                                    $(".viewAbsenceComment").prop('disabled', true);

                                } else if (absence.status == "Approved") {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset()*60*1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset()*60*1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('.viewAbsenceComment').val(absence.comment);
                                    $('#viewChbPrivate').prop('checked', value = absence.privateComment);
                                    $('#viewApprovedAbsenceModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);
                                    $(".viewAbsenceComment").prop('disabled', true);
                                } else {
                                    var startDate = new Date(absence.startDate);
                                    var endDate = new Date(absence.endDate);

                                    startDate.setTime(startDate.getTime() + new Date(startDate).getTimezoneOffset()*60*1000);
                                    endDate.setTime(endDate.getTime() + new Date(endDate).getTimezoneOffset()*60*1000);

                                    $('.startDateViewer').datepicker('update', startDate);
                                    $('.endDateViewer').datepicker('update', endDate);
                                    $('.viewAbsenceComment').val(absence.comment);
                                    $('#viewChbPrivate').prop('checked', value = absence.privateComment);
                                    $('#approverRequestModal').modal('toggle');

                                    $("span").css("pointer-events", "none");
                                    $('.startDateViewer > .form-control').prop('disabled', true);
                                    $('.endDateViewer > .form-control').prop('disabled', true);
                                    $(".viewAbsenceComment").prop('disabled', true);
                                }
                            });
                    }
                });
        }
    });

    $('#startDatePicker')
        .datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            startDate: new Date(),
            weekStart: 1,
            forceParse: false
        });

    $('#endDatePicker')
        .datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            startDate: new Date(),
            weekStart: 1,
            forceParse: false
        });

    $('#startDateUpdater')
        .datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            startDate: new Date(),
            weekStart: 1,
            forceParse: false
        });

    $('#endDateUpdater')
        .datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            startDate: new Date(),
            weekStart: 1,
            forceParse: false
        });

    $('.startDateViewer')
        .datepicker({
            format: 'dd-mm-yyyy'
        });

    $('.endDateViewer')
        .datepicker({
            format: 'dd-mm-yyyy'
        });

    $("#startDatePicker").datepicker().on("changeDate", function () {
        checkAbsenceValidity();
    });

    $("#endDatePicker").datepicker().on("changeDate", function () {
        checkAbsenceValidity();
    });

    $("#startDateUpdater").datepicker().on("changeDate", function () {
        checkAbsenceValidity();
    });

    $("#endDateUpdater").datepicker().on("changeDate", function () {
        checkAbsenceValidity();
    });

    $(".btn-opret").on("click", function () {
        if ($(".calendar-content").attr("data-id") == $(".calendar-content").attr("data-approver")) {
            createAbsence("Approved");
        } else {
            createAbsence("Pending");
        }
    });

    $("#btn-opdaterApprover").on("click", function () {
        updateAbsenceApprover();
    });

    $("#btn-opdaterUser").on("click", function () {
        updateAbsenceUser();
    });

    $(".btn-slet").on("click", function () {
        deleteAbsence();
    });

    $(".btn-godkend").on("click", function () {
        approveAbsence();
    });

    $(".btn-afvis").on("click", function () {
        declineAbsence();
    });

    $(".btn-anmodSlet").on("click", function () {
        $.post('/absence/requestdelete', {
            user: selectedUser,
            absence: $(calendarDay).attr('data-absence')
        })
            .done(function () {})
            .fail(function (err) {
                console.log("Error: " + err);
            });

    });

});

function checkAbsenceValidity() {
    var validAbsence = false;
    var startDate;
    var endDate;

    if ($('#updateAbsenceModal').hasClass("in")) {
        startDate = $("#startDateUpdater").data().datepicker.viewDate;
        endDate = $("#endDateUpdater").data().datepicker.viewDate;
    } else {
        startDate =  $("#startDatePicker").data().datepicker.viewDate;
        endDate = $("#endDatePicker").data().datepicker.viewDate;
    }

    $.post('/absence/validity', {
        user: selectedUser,
        absence: $(calendarDay).attr('data-absence'),
        startDate: startDate,
        endDate: endDate
    })
        .done(function (data) {
            validAbsence = data;
            if (validAbsence && $("#startDatePicker").data().datepicker.dates.length != 0 && $("#endDatePicker").data().datepicker.dates.length != 0 && $("#endDatePicker").data().datepicker.viewDate >= $("#startDatePicker").data().datepicker.viewDate) {
                $(".btn-opret").prop('disabled', false);
            } else {
                $(".btn-opret").prop('disabled', true);
            }
            if (validAbsence && $("#startDateUpdater").data().datepicker.dates.length != 0 && $("#endDateUpdater").data().datepicker.dates.length != 0 && $("#endDateUpdater").data().datepicker.viewDate >= $("#startDateUpdater").data().datepicker.viewDate) {
                $("#btn-opdaterApprover").prop('disabled', false);
            } else {
                $("#btn-opdaterApprover").prop('disabled', true);
            }
        })
        .fail(function (err) {
            console.log("Error: " + err);
        });
}

function createAbsence(status) {
    var now = new Date();
    now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    $.post('/absence', {
        user: selectedUser,
        startDate: $("#startDatePicker").data().datepicker.viewDate,
        endDate: $("#endDatePicker").data().datepicker.viewDate,
        status: status,
        comment: $("#absenceComment").val(),
        privateComment: $("#chbPrivate").is(":checked"),
        requestDate: now
    })
        .done(function () {
            updateView();
        })
        .fail(function (err) {
            console.log("Error: " + err);
        });
}

function updateAbsenceApprover() {
    var now = new Date();
    now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    $.ajax({
        url: '/absence/approver',
        type: 'PUT',
        data: {
            user: selectedUser,
            absence: $(calendarDay).attr('data-absence'),
            startDate: $("#startDateUpdater").data().datepicker.viewDate,
            endDate: $("#endDateUpdater").data().datepicker.viewDate,
            comment: $("#updateAbsenceCommentApprover").val(),
            privateComment: $("#updateChbPrivateApprover").is(":checked"),
            requestDate: now
        }
    })
        .then(
            function success() {
                updateView();
            },

            function fail(err) {
                console.log("Error: " + err);
            }
        );
}

function updateAbsenceUser() {
    var now = new Date();
    now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    $.ajax({
        url: '/absence/user',
        type: 'PUT',
        data: {
            user: selectedUser,
            absence: $(calendarDay).attr('data-absence'),
            comment: $("#updateAbsenceCommentUser").val(),
            privateComment: $("#updateChbPrivateUser").is(":checked"),
            requestDate: now
        }
    })
        .then(
            function success() {
                updateview();
            },

            function fail(err) {
                console.log("Error: " + err);
            }
        );
}

function approveAbsence() {
    var now = new Date();
    now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    $.ajax({
        url: '/absence/approve',
        type: 'PUT',
        data: {
            absence: $(calendarDay).attr('data-absence')
        }
    })
        .then(
            function success() {
                updateView();
            },

            function fail(err) {
                console.log("Error: " + err);
            }
        );
}

function declineAbsence() {
    var now = new Date();
    now.setTime(now.getTime() - new Date().getTimezoneOffset()*60*1000);

    $.ajax({
        url: '/absence/decline',
        type: 'PUT',
        data: {
            absence: $(calendarDay).attr('data-absence')
        }
    })
        .then(
            function success() {
                updateView();
            },

            function fail(err) {
                console.log("Error: " + err);
            }
        );
}

function deleteAbsence() {
    $.ajax({
        url: '/absence',
        type: 'DELETE',
        data: {
            user: selectedUser,
            absence: $(calendarDay).attr('data-absence')
        }
    })
        .then(
            function success() {
                updateView();
            },

            function fail(err) {
                console.log("Error: " + err);
            }
        );
}
