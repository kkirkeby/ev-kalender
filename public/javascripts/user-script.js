$(document).ready(function () {
    var userId = $('.user-content').attr('data-id');

    var appendMyAbsences = function () {
        $.get('api/users/' + userId)
            .done(function (user) {
                $('#myAbsences').append('<thead><th>Forespørgselstidspunkt</th><th>Varighed</th><th>Kommentar</th><th>Status</th></thead><tbody>');
                for (var i = 0; i < user.absences.length; i++) {
                    $.get('api/absence/' + user.absences[i])
                        .done(function (absence) {
                            if (absence != null) {
                                   var requestDate = moment(absence.requestDate).subtract(2, 'hours').format("DD-MM-YYYY HH:mm");
                                   var startDate = moment(absence.startDate).subtract(2, 'hours').format("DD-MM-YYYY");
                                   var endDate = moment(absence.endDate).subtract(2, 'hours').format("DD-MM-YYYY");

                                var formattedStatus = "";
                                if (absence.status == "Approved") {
                                    formattedStatus = "Godkendt";

                                } else if (absence.status == "Declined") {
                                    formattedStatus = "Afvist";
                                } else {
                                    formattedStatus = "Afventer behandling"
                                }

                                $('#myAbsences').append('<tr id="absence_' + absence._id + '"><td class="text-left">' + requestDate + '</td><td class="text-left">' + startDate + ' - ' + endDate + '</td><td class="text-left">' + absence.comment + '</td><td class="text-left">' + formattedStatus + '</td></tr>');
                                if (absence.status == "Approved") {
                                    $('#absence_' + absence._id).attr("class", "success");
                                } else if (absence.status == "Declined") {
                                    $('#absence_' + absence._id).attr("class", "danger");
                                } else {
                                    $('#absence_' + absence._id).attr("class", "warning");
                                }
                            }
                        });
                }
                $('#myAbsences').append('</tbody>');
            });
    };
    appendMyAbsences();
});