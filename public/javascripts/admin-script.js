var registerUser = function (username, password, firstName, surName, email, approver, rights) {
    $.post('api/users', {
        "username": username,
        "password": password,
        "firstName": firstName,
        "surName": surName,
        "email": email,
        "approver": approver,
        "rights": rights
    })
        .done(function (result) {
            var alert = $('.alertModal');
            alert.find('.modal-body').append(result);
            alert.modal('toggle');
        })
        .catch(function (err) {
            if (err) {
                var alert = $('.alertModal');
                alert.find('.modal-body').append('Fejl: ' + err);
                alert.modal('toggle');
            }
        });
};

var username = $('#registerUsername');
var password = $('#registerPassword');
var firstname = $('#registerFirstName');
var surname = $('#registerSurName');
var email = $('#registerEmail');
var approver = $('#registerApprover');
var admin = $('#registerAdmin');
var adminRights = 0;

var acceptedInputs = [false, false, false, false, false, false];

username.on('keyup', function () {
    // Validering af brugernavn - minimum 3 karakterer, maksimum 10 og må kun indeholde a-z æøå
    var validate_username = /^[a-zæøå]{3,10}$/gi
    if (!validate_username.test(username.val())) {
        acceptedInputs[0] = false;
        username.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[0] = true;
        username.closest('.form-group').removeClass('has-error');
    }
});

password.on('keyup', function () {
    // Validering af password - minimum 8 karakterer og mindst et bogstav og et tal
    var validate_password = /^(?=.*[a-zæøå])(?=.*\d)[a-zæøå\d]{8,}$/gi
    if (!validate_password.test(password.val())) {
        acceptedInputs[1] = false;
        password.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[1] = true;
        password.closest('.form-group').removeClass('has-error');
    }
});

firstname.on('keyup', function () {
    // Validering af fornavn - minimum 2 karakterer, maksimum 32 og må kun indeholde a-z æøå
    var validate_firstname = /^[a-zæøå]{2,32}$/gi
    if (!validate_firstname.test(firstname.val())) {
        acceptedInputs[2] = false;
        firstname.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[2] = true;
        firstname.closest('.form-group').removeClass('has-error');
    }
});

surname.on('keyup', function () {
    // Validering af efternavn - minimum 2 karakterer, maksimum 32 og må kun indeholde a-z æøå
    var validate_surname = /^[a-zæøå]{2,32}$/gi
    if (!validate_surname.test(surname.val())) {
        acceptedInputs[3] = false;
        surname.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[3] = true;
        surname.closest('.form-group').removeClass('has-error');
    }
});

email.on('keyup', function () {
    // Validering af email - General Email Regex (RFC 5322 Official Standard)
    var validate_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gi
    if (!validate_email.test(email.val())) {
        acceptedInputs[4] = false;
        email.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[4] = true;
        email.closest('.form-group').removeClass('has-error');
    }
});

approver.on('click', function () {
    if (approver.val() == "no-selection" && !admin.is(':checked')) {
        acceptedInputs[5] = false;
        approver.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[5] = true;
        approver.closest('.form-group').removeClass('has-error');
    }
});

admin.on('click', function () {
    if (approver.val() == "no-selection" && !admin.is(':checked')) {
        acceptedInputs[5] = false;
        approver.closest('.form-group').addClass('has-error');
    } else {
        acceptedInputs[5] = true;
        approver.closest('.form-group').removeClass('has-error');
    }
});

$('#registerUser').click(function (event) {
    event.preventDefault();

    var acceptRegister = true;
    for (var i = 0; i < acceptedInputs.length; i++) {
        if (acceptedInputs[i] == false) {
            acceptRegister = false;
        }
    }

    if (acceptRegister) {
        // Sætter brugerrettigheder afhængig af checkbox
        if (admin[0].checked) {
            adminRights = 1;
        } else {
            adminRights = 0;
        }

        acceptedInputs = [false, false, false, false, false];

        registerUser(username.val(), password.val(), firstname.val(), surname.val(), email.val(), approver.val(), adminRights);
        // Nulstil inputfelter
        username.val('');
        password.val('');
        firstname.val('');
        surname.val('');
        email.val('');
        approver.val('no-selection');
        admin.prop("checked", false);
    } else {
        var alert = $('.alertModal');
        alert.find('.modal-body').append('Der er felter der mangler at blive udfyldt!');
        alert.modal('toggle');
    }
});

$(document).ready(function () {
    // Nulstiller modal ved lukning
    $('.alertModal').on('hidden.bs.modal', function () {
        $(this).find('.modal-body').html("");
    });

    var appendAdmins = function () {
        $.get('api/admin')
            .done(function (admins) {
                var approver = $('#registerApprover');
                for (var i = 0; i < admins.length; i++) {
                    approver.append('<option value=' + admins[i]._id + '>' + admins[i].username + '</option>');
                }
            });
    };
    appendAdmins();

    var adminId = $('.admin-content').attr('data-id');

    var appendUsersByApprover = function () {
        $.get('api/users/admin/' + adminId)
            .done(function (users) {
                var usersByApprover = $('#usersByApprover');
                // Indsætter brugere i hver sit panel
                for (var i = 0; i < users.length; i++) {
                    usersByApprover.append('<div id="user_' + users[i]._id + '" class="panel-group">' +
                        '<div class="panel panel-primary" id="user_' + users[i]._id + '">' +
                        '<div class="panel-heading">' +
                        '<h4 class="panel-title"><a data-toggle="collapse" data-parent="#user_' + users[i]._id + '" href="#collapseuser_' + users[i]._id + '">' + users[i].firstName + ' ' + users[i].surName + '</a></div>' +
                        '<div id="collapseuser_' + users[i]._id + '" class="panel-collapse collapse in">' +
                        // Indsætter table med table headers og body for hver bruger
                        '<div class="panel-body"><div class="table-responsive"><table class="table table-striped table-hover" id="absenceuser_' + users[i]._id + '"><thead><tr><th>Forespørgselstidspunkt</th><th>Varighed</th><th>Kommentar</th><th>Godkend</th><th>Afvis</th></tr></thead><tbody></tbody></table></div>');

                    // Gennemløber ferieønsker for brugeren og indsætter dem i tabellen
                    for (var j = 0; j < users[i].absences.length; j++) {
                        // Indsætter table row til ferieønske
                        $('#absenceuser_' + users[i]._id).append('<tr id="absence_' + users[i].absences[j] + '"></tr>');
                        // Indsætter information om de enkelte ferieønsker
                        appendAbsences(users[i]._id, users[i].absences[j]);
                    }
                }
            });
    };
    appendUsersByApprover();

    // Henter ferieønsker og indsætter dem i tabellen
    var appendAbsences = function (userId, absenceId) {
        $.get('api/absence/' + absenceId)
            .done(function (absence) {
                    var absencesByUser = $('#absence_' + absenceId);
                    if (absence != null) {
                        // Ferieønsket skal være "Pending"
                        if (absence.status == "Pending") {
                            var startDate = moment(absence.startDate).subtract(2, 'hours').format("DD-MM-YYYY");
                            var endDate = moment(absence.endDate).subtract(2, 'hours').format("DD-MM-YYYY");
                            var requestDate = moment(absence.requestDate).subtract(2, 'hours').format("DD-MM-YYYY HH:mm");
                            absencesByUser.append('<td class="text-left">' + requestDate + '</td>');
                            absencesByUser.append('<td class="text-left">' + startDate + ' - ' + endDate + '</td>');
                            absencesByUser.append('<td class="text-left">' + absence.comment + '</td>');
                            absencesByUser.append('<td class="text-left"><button style="margin: 2px" class="btn btn-success" id="approveAbsence_' + absenceId + '">Godkend</button></>');
                            absencesByUser.append('<td class="text-left"><button style="margin: 2px" class="btn btn-danger" id="declineAbsence_' + absenceId + '">Afvis</button></>');

                            // Clickhandler til godkendelse af ferieønske
                            $('#approveAbsence_' + absenceId).on('click', function (event) {
                                event.preventDefault();
                                updateAbsenceStatus(absenceId, 1);
                                $('#absence_' + absenceId).remove();
                            });
                            // Clickhandler til afvisning af ferieønske
                            $('#declineAbsence_' + absenceId).on('click', function (event) {
                                event.preventDefault();
                                updateAbsenceStatus(absenceId, 2);
                                $('#absence_' + absenceId).remove();
                            });

                        }
                    }
                }
            )
    };

    var updateAbsenceStatus = function (absenceId, status) {
        $.ajax({
            url: 'api/absence/' + absenceId,
            type: 'PUT',
            data: {
                status: status
            }
        })
            .then(
                function success(absence) {
                    // console.log("Absence updated!");
                    // console.log(absence);
                },
                function fail(err) {
                    console.log("Error: " + err);
                }
            );
    };

    var appendUsers = function () {
        $.get('api/users')
            .done(function (users) {
                var userAdmin = $('#userAdministration');
                userAdmin.append('<div class="table-responsive"><table class="table table-striped table-hover" id="userAdministrationTable"><thead><tr><th>Fornavn</th><th>Efternavn</th><th>Brugernavn</th><th>E-mail</th><th>Aktiv</th><th>Rettigheder</th><th>Slet</th></tr></thead><tbody></tbody></table></div>');
                var userAdminTable = $('#userAdministrationTable');
                for (var i = 0; i < users.length; i++) {
                    var userId = users[i]._id;

                    var active = "";
                    if (users[i].active == true) {
                        active = "Aktiv";
                    } else {
                        active = "Inaktiv";
                    }
                    var rights = "";
                    if (users[i].rights == 1) {
                        rights = "Admin (1)";
                    } else {
                        rights = "Bruger (0)";
                    }

                    userAdminTable.append('<tr id="userId_' + userId + '"><td class="text-left">' + users[i].firstName + '</td><td class="text-left">' + users[i].surName + '</td><td class="text-left">' + users[i].username + '</td><td class="text-left">' + users[i].email + '</td><td class="text-left">' + active + '</td><td class="text-left">' + rights + '</td><td class="text-left"><button data-userid="' + userId + '" class="deleteUserBtn btn btn-danger">Slet bruger</button></td></tr>');
                    userAdminTable.append('<div data-userid="' + userId + '" class="confirmDelete modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Slet bruger</h4></div><div class="modal-body">Vil du slette brugeren ' + users[i].firstName + ' ' + users[i].surName + ' (' + users[i].username + ')?</div><div class="modal-footer"><button type="button" data-dismiss="modal" data-userid="' + userId + '" class="confirmDeleteBtn btn btn-danger">SLET</button><button type="button" data-dismiss="modal" class="btn btn-default">Fortryd</button></div></div></div></div>');

                }
                $('.deleteUserBtn').on('click', function (event) {
                    event.preventDefault();
                    $('.confirmDelete[data-userid = ' + $(this).attr('data-userid') + ']').modal({
                        backdrop: 'static',
                        keyboard: false
                    }).on('click', '.confirmDeleteBtn', function (event) {
                        event.preventDefault();
                        var userId = $(this).attr('data-userid');
                        console.log('Brugerid slettet: ' + userId);
                        deleteUser(userId);
                        $('#userId_' + userId).remove();
                    });
                });
            });
    };
    appendUsers();

    var deleteUser = function (userId) {
        $.ajax({
            url: 'api/users/' + userId,
            type: 'DELETE',
            data: {}
        })
            .then(
                function success(removed) {
                    var alert = $('.alertModal');
                    alert.find('.modal-body').append('Bruger slettet!');
                    alert.modal('toggle');
                },
                function fail(err) {
                    console.log("Error: " + err);
                }
            );
    };
});