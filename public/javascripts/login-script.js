$(document).ready(function () {
    var loginUser = function (inputUsername, inputPassword) {
        $.post('api/login', {username: inputUsername, password: inputPassword})
            .done(function (response) {
                if (response === 1) {
                    $('#loginPasswordError').append('<p>Forkert adgangskode!</p>');
                } else if (response === 2) {
                    $('#loginUsernameError').append('<p>Forkert brugernavn!</p>');
                } else {
                    window.location.replace('/')
                }
                console.log(response);
            })
            .fail(function () {
                console.log("$.POST Error");
                // console.log(err);
                // console.log('$.POST error');
            })
    };

    $('#login').on('click', function (event) {
        event.preventDefault();
        var username = $('#loginUsername');
        var password = $('#loginPassword');
        loginUser(username.val(), password.val());
        username.val('');
        password.val('');
        $('#loginUsernameError').empty();
        $('#loginPasswordError').empty();
    });
});