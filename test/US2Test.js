// var app = require('./../app.js');
// var supertest = require('supertest');
// var Absence = require('../models/Absence');
// var User = require('../models/User');
//
// suite('testing root contents', function() {
//     test('get absence"', function(done) {
//         supertest(app)
//             .get('/absence')
//             .expect('Content-Type', /json/)
//             .expect(200, done);
//     });
//
//     test('post absence"', function(done) {
//         var absence1 = new Absence({
//             user: user,
//             startDate: new Date(2017, 05, 05),
//             endDate: new Date(2017, 05, 05),
//             requestDate: Date.now()
//         });
//
//         var absence2 = new Absence({
//             user: user,
//             startDate: new Date(2017, 05, 06),
//             endDate: new Date(2017, 05, 06),
//             requestDate: Date.now()
//         });
//
//         var absence3 = new Absence({
//             user: user,
//             startDate: new Date(2017, 05, 07),
//             endDate: new Date(2017, 05, 07),
//             requestDate: Date.now()
//         });
//
//         var user = new User({
//             username: "US2 Test User",
//             password: "1234",
//             absences: [absence1, absence2, absence3],
//             _id: "590c4d7d3ade8c1e8014ad97"
//         });
//
//         //Må gerne oprettes da ligger uden for al anden ferie
//         var start = new Date(2017, 7, 3);
//         var end = new Date(2017, 7, 7);
//         supertest(app)
//             .post('/absence')
//             .send({"user": user, "startDate": start, "endDate": end, "requestDate": Date.now()})
//             .expect('Content-Type', /json/)
//             .expect(200, done);
//
//         //Må ikke oprettes da ligger oven i anden ferie
//         var start2 = new Date(2017, 5, 3);
//         var end2 = new Date(2017, 5, 7);
//         supertest(app)
//             .post('/absence')
//             .send({"user": user, "startDate": start2, "endDate": end2, "requestDate": Date.now()})
//             .expect('Content-Type', /text/)
//             .expect(500, done);
//     });
// });
