var bodyParser = require("body-parser");
var express = require('express');
var app = express();
var session = require('express-session');
var controller = require("./controllers/controller");

app.set('port', (process.env.PORT || 8080)); // Set the port
app.use(session({secret: '109h50TcRwel034hj9088Za', saveUninitialized: true, resave: true}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

// MONGODB & MONGOOSE SETUP
// =============================================================================
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://admin:ferie1234@ds135089.mlab.com:35089/ev-kalender');

var sess;

app.post('/gen', function (req, res) {
    if (req.body.password == "3409fvEW") {
        controller.registerUser('knr', '1234abcd', 'Karen', 'Ravnløkke', 'evkalendersystem@gmail.com', '.', 1)
            .then(function (user) {
                var adminUserId = user._id;
                controller.registerUser('admin', '123', 'Admin', 'istrator', 'evkalendersystem@gmail.com', '.', 1);
                controller.registerUser('kar', '1234abcd', 'Krestina', 'Rasmussen', 'evkalendersystem@gmail.com', adminUserId, 0);
                controller.registerUser('krk', '1234abcd', 'Kristoffer', 'Kirkeby', 'evkalendersystem@gmail.com', adminUserId, 0);
                controller.registerUser('dms', '1234abcd', 'Dorte', 'Skovfoged', 'evkalendersystem@gmail.com', adminUserId, 0);
                controller.registerUser('ser', '1234abcd', 'Susanne', 'Rasmussen', 'evkalendersystem@gmail.com', adminUserId, 0);
                res.send('Dummy users created!');
            })
            .catch(function (err) {
                res.status(500).send('Fejl: ' + err);
            });
    } else {
        res.send('Generate users not working!');
    }
});

exports.auth = function (req, res, next) {
    sess = req.session;
    if (sess.user) {
        next();
    } else {
        res.redirect('/');
    }
};

exports.authAdmin = function (req, res, next) {
    sess = req.session;
    if (sess.user !== undefined && sess.user.rights === 1) {
        next();
    } else {
        res.redirect('/');
    }
};

exports.authRoute = function (req, res, next) {
    sess = req.session;
    if (sess.user) {
        next();
    } else {
        res.send('Route requires login!');
    }
};

exports.authRouteAdmin = function (req, res, next) {
    sess = req.session;
    if (sess.user !== undefined && sess.user.rights === 1) {
        next();
    } else {
        res.send('Route requires login with admin rights!');
    }
};

// ROUTES FOR THE APP
// =============================================================================
var loginRouter = require('./routes/login')(express);
var absenceRouter = require('./routes/absence')(express);
var apiRouter = require('./routes/api')(express);
var calendarRouter = require('./routes/calendar')(express);
var adminRouter = require('./routes/admin')(express);
var userRouter = require('./routes/user')(express);

app.use(loginRouter);
app.use(absenceRouter);
app.use(apiRouter);
app.use(calendarRouter);
app.use(adminRouter);
app.use(userRouter);

// START THE SERVER
// =============================================================================
app.listen(app.get('port'), function () {
    console.log("Server running at http://localhost:8080");
});

// Export for use in tests
module.exports = app;