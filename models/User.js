var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var Absence = require('./Absence');

SALT_WORK_FACTOR = 10;

var userSchema = new Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    firstName: {type: String, required: true},
    surName: {type: String, required: true},
    email: {type: String, required: true},
    approver: {type: String, required: true},
    active: {type: Boolean, default: true},
    rights: {type: Number, default: 0},
    absences : [{type: Schema.Types.ObjectId, ref: 'Absence' }]
});

userSchema.pre('save', function (next) {
    var user = this;

    // Hash kun password hvis password er nyt eller modified
    if (!user.isModified('password')) return next();

    // Generer en salt værdi
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // Hash password med salt værdien
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // Sæt password til den nye hash
            user.password = hash;
            next();
        });
    });
});

userSchema.pre('remove', function (next) {
    Absence.remove({user: this._id}).exec();
    next();
});

userSchema.methods.comparePassword = function(passwordAttempt, callback) {
    bcrypt.compare(passwordAttempt, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};

userSchema.methods.toString = function () {
    return this.username;
};

module.exports = mongoose.model('User', userSchema);