var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var absence = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    startDate: {type: Date, required: true},
    endDate: {type: Date, required: true},
    status: {type: String, required: true, enum: ["Pending", "Declined", "Approved"]},
    comment: {type: String},
    privateComment: {type: Boolean},
    requestDate: {type: Date, required: true}
});

module.exports = mongoose.model('Absence', absence);