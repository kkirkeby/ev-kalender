var app = require('./../app');
var controller = require("../controllers/controller");

module.exports = function (express) {
    var router = express.Router();
    var sess;

    // Render Routes
    router.route('/admin')
        .get(app.authAdmin, function (req, res) {
            sess = req.session;
            res.render('admin', {
                title: 'Administration',
                header: 'Administration',
                user: sess.user
            })
        });

    return router;
};