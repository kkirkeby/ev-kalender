var app = require('./../app');
var controller = require("../controllers/controller");

module.exports = function (express) {
    var router = express.Router();
    var sess;

    // Render Routes
    router.route('/user')
        .get(app.auth, function (req, res) {
            sess = req.session;
            res.render('user', {
                title: 'Bruger',
                header: 'Bruger',
                user: sess.user
            })
        });

    return router;
};