var controller = require("../controllers/controller");
var app = require('./../app');

module.exports = function (express) {

    var router = express.Router();

    router.route('/api/absence/:from/:to')
        .get(app.authRoute, function (req, res) {
            controller.getAbsences2(req.params.from, req.params.to)
                .then(function (absences) {
                    res.json(absences);
                }).catch(function (err) {
                res.status(404).send(err);
            });
        });

    router.route('/api/users/admin/:approver')
        .get(app.authRouteAdmin, function (req, res) {
            controller.getUsersByApprover(req.params.approver)
                .then(function (users) {
                    res.json(users);
                }).catch(function (err) {
                res.send(err);
            })
        });

    router.route('/api/absence/:id')
        .get(app.authRoute, function (req, res) {
            controller.getAbsence(req.params.id)
                .then(function (absence) {
                    res.json(absence);
                }).catch(function (err) {
                res.send(err);
            })
        })
        .put(app.authRoute, function (req, res) {
            controller.updateAbsenceStatus(req.params.id, req.body.status)
                .then(function (absence) {
                    res.json(absence);
                })
                .catch(function (err) {
                    res.send(err);
                });
        });

    router.route('/api/login')
        .post(function (req, res) {
            var username = req.body.username;
            var password = req.body.password;
            controller.userLogin(username, password)
                .then(function (user) {
                    sess = req.session;
                    sess.user = user;
                    res.json(user);
                })
                .catch(function (err) {
                    res.json(err);
                });
        });

    router.route('/api/users')
        .get(app.authRoute, function (req, res) {
            controller.getUsers()
                .then(function (users) {
                    res.json(users);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).send(err);
                });
        })
        .post(app.authRouteAdmin, function (req, res) {
            controller.registerUser(req.body.username, req.body.password, req.body.firstName, req.body.surName, req.body.email, req.body.approver, req.body.rights)
                .then(function (user) {
                    if (user.rights == 1) {
                        controller.setSelfApprove(user._id)
                            .then(function () {
                                res.send('Admin oprettet');
                            })
                            .catch(function (err) {
                                res.status(500).send('Fejl: ' + err);
                            });
                    } else {
                        res.send('Bruger oprettet');
                    }
                })
                .catch(function (err) {
                    if (err.code === 11000) {
                        res.send('Fejl: Brugernavnet eksisterer allerede i databasen');
                    } else {
                        res.status(500).send('Fejl: ' + err);
                    }
                });
        });

    router.route('/api/users/:id')
        .get(app.authRoute, function (req, res) {
            controller.getUser(req.params.id)
                .then(function (user) {
                    res.json(user);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).send(err);
                });
        })
        .delete(app.authRouteAdmin, function (req, res) {
            controller.deleteUser(req.params.id)
                .then(function (removed) {
                    // Returnerer true hvis brugeren blev slettet, og false hvis ikke
                    res.send(removed);
                })
                .catch(function (err) {
                    res.send(err);
                });
        });

    router.route('/api/admin')
        .get(app.authRouteAdmin, function (req, res) {
            controller.getAdmins()
                .then(function (admins) {
                    res.json(admins);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).send(err);
                });
        });

    return router;

};






