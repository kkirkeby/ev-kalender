var controller = require("../controllers/controller");
var app = require('./../app');

module.exports = function(express) {
    var router = express.Router();
    router.route('/absence')
        .get(app.authRoute, function (req, res) {
            controller.getAbsences()
                .then(function(val) {
                    res.json(val);
                })
                .catch(function(err) {
                    console.log(err);
                    res.status(500).send(err);
                });
        })
        .post(app.authRoute, function (req, res) {
            controller.createAbsence(req.body.user, req.body.startDate, req.body.endDate, req.body.status, req.body.comment, req.body.privateComment, req.body.requestDate)
                .then(function() {
                    res.json({message: 'Absence saved!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        })
        .delete(app.authRoute, function (req, res) {
            controller.deleteAbsence(req.body.user, req.body.absence)
                .then(function() {
                    res.json({message: 'Absence deleted!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/validity')
        .post(app.authRoute, function (req, res) {
            controller.checkAbsenceForValidity(req.body.user, req.body.absence, req.body.startDate, req.body.endDate)
                .then(function(data) {
                    res.json(data);
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/approver')
        .put(app.authRoute, function (req, res) {
            controller.updateAbsenceApprover(req.body.absence, req.body.startDate, req.body.endDate, req.body.comment, req.body.privateComment, req.body.requestDate)
                .then(function() {
                    controller.absenceUpdatedMail(req.body.user, req.body.absence);
                    res.json({message: 'Absence updated!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/user')
        .put(app.authRoute, function (req, res) {
            controller.updateAbsenceUser(req.body.absence, req.body.comment, req.body.privateComment, req.body.requestDate)
                .then(function() {
                    controller.absenceUpdatedMail(req.body.user, req.body.absence);
                    res.json({message: 'Absence updated!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/approve')
        .put(app.authRouteAdmin, function (req, res) {
            controller.updateAbsenceStatus(req.body.absence, 1)
                .then(function() {
                    res.json({message: 'Absence approved!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/decline')
        .put(app.authRouteAdmin, function (req, res) {
            controller.updateAbsenceStatus(req.body.absence, 2)
                .then(function() {
                    res.json({message: 'Absence declined!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    router.route('/absence/requestdelete')
        .post(app.authRoute, function (req, res) {
            controller.requestDeleteMail(req.body.user, req.body.absence)
                .then(function() {
                    res.json({message: 'Request sent!'});
                })
                .catch(function(err) {
                    console.error("Error: " + err);
                    if (err.stack) console.error(err.stack);
                    res.status(500).send(err);
                });
        });

    return router;
};