var app = require('./../app');
var controller = require("../controllers/controller");


module.exports = function (express) {
    var router = express.Router();
    var sess;

    router.route('/')
        .get(function (req, res) {
            sess = req.session;
            if (sess.user) {
                res.redirect('/calendar');
            }
            else {
                res.redirect('/login');
            }
        });

    router.route('/calendar')
        .get(app.auth, function (req, res) {
            sess = req.session;
            controller.getUsers()
                .then(function (users) {
                    res.render('calendar', {title: 'EV Kalender', date: new Date().toLocaleDateString(), users, user: sess.user})
                })
                .catch(function (err) {
                    res.render('calendar', {
                        date: new Date().toLocaleDateString()
                    });
                });
        });
    return router;
};