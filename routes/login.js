var controller = require("../controllers/controller");
var session = require('express-session');
var app = require('./../app');

var sess;

module.exports = function (express) {
    var router = express.Router();

    // Render Routes
    router.route('/login')
        .get(function (req, res) {
            res.render('login', {
                title: 'EV Kalender Login',
                header: 'EV Kalender Login',
            })
        });

    router.route('/logout')
        .get(function (req, res) {
            req.session.destroy(function (err) {
                if (err) {
                    console.log(err);
                }
                else {
                    res.redirect('/');
                }
            });
        });
    return router;
};