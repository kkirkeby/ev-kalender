var Absence = require('../models/Absence');
var User = require('../models/User');
var nodemailer = require('nodemailer');
var moment = require('moment');

var sendMail = function (mailTo, username, status, requestDate, startDate, endDate, comment) {
    moment.locale('da');
    var requestDateAsDateFormat = new Date(requestDate);
    var formattedRequestDate = moment.utc(requestDateAsDateFormat).format('DD-MM-YYYY HH:mm:ss');
    var startDateAsDateFormat = new Date(startDate);
    var formattedStartDate = moment.utc(startDateAsDateFormat).format('DD-MM-YYYY');
    var endDateAsDateFormat = new Date(endDate);
    var formattedEndDate = moment.utc(endDateAsDateFormat).format('DD-MM-YYYY');
    var formattedStatus = "";
    var subject = "";
    if (status == "Pending") {
        formattedStatus = "Afventer behandling";
        subject = "EV-kalender - Kvittering for ferieforespørgsel";
    } else if (status == "Declined") {
        formattedStatus = "Afvist";
        subject = "EV-kalender - Dit ferieønske er afvist";
    } else if (status == "Approved") {
        formattedStatus = "Godkendt";
        subject = "EV-kalender - Dit ferieønske er godkendt";
    } else {
        formattedStatus = "Ukendt";
        subject = "EV-kalender";
    }

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'evkalendersystem@gmail.com',
            pass: 'r8Dmc#9Q'
        }
    });

    var mailOptions = {
        from: '"EV-kalender" <evkalendersystem@gmail.com>',
        to: mailTo,
        subject: subject,
        text: 'Brugernavn: ' + username + ', Status: ' + formattedStatus + ', Forespørgselstidspunkt: ' + formattedRequestDate + ', Varighed: ' + formattedStartDate + ' - ' + formattedEndDate + ', Kommentar:' + comment,
        html: '<table border="1"><tr><th align="left">Brugernavn</th><td>' + username + '</td></tr><tr><th align="left">Status</th><td>' + formattedStatus + '</td></tr><tr><th align="left">Forespørgselstidspunkt</th><td>' + formattedRequestDate + '</td></tr><tr><th align="left">Varighed</th><td>' + formattedStartDate + ' - ' + formattedEndDate + '</td></tr><tr><th align="left">Kommentar</th><td>' + comment + '</td></tr></tr></table>'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return error;
        } else {
            return 'Message sent';
        }
    });
};

exports.requestDeleteMail = function (user, absence) {
    return new Promise(function (resolve, reject) {

        Absence
            .findById(absence)
            .exec(function (err, absence) {
                if (err) reject(err);

                moment.locale('da');
                var startDateAsDateFormat = new Date(absence.startDate);
                var formattedStartDate = moment.utc(startDateAsDateFormat).format('DD-MM-YYYY');
                var endDateAsDateFormat = new Date(absence.endDate);
                var formattedEndDate = moment.utc(endDateAsDateFormat).format('DD-MM-YYYY');

                var privatKommentar;

                if (absence.privateComment) {
                    privatKommentar = "ja";
                } else {
                    privatKommentar = "nej"
                }

                User
                    .findById(user)
                    .exec(function (err, user) {
                        if (err) reject(err);

                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'evkalendersystem@gmail.com',
                                pass: 'r8Dmc#9Q'
                            }
                        });

                        var mailOptions = {
                            from: user.email,
                            to: '"EV-kalender" <evkalendersystem@gmail.com>',
                            subject: "EV-kalender - " + user.username + " anmoder om sletning af ferieønske",
                            text: 'Brugernavn: ' + user.username + ', Varighed: ' + formattedStartDate + ' - ' + formattedEndDate + ', Kommentar:' + absence.comment + ', Privat:' + absence.privateComment,
                            html: '<table border="1"><tr><th align="left">Brugernavn</th><td>' + user.username + '</td></tr><tr><th align="left">Varighed</th><td>' + formattedStartDate + ' - ' + formattedEndDate + '</td></tr><tr><th align="left">Kommentar</th><td>' + absence.comment + '</td></tr></tr><tr><th align="left">Privat</th><td>' + privatKommentar + '</td></tr></tr></table>'
                        };

                        transporter.sendMail(mailOptions, function (error) {
                            if (error) {
                                return error;
                            } else {
                                return 'Message sent';
                            }
                        });
                    })
            })
    })
};

exports.absenceUpdatedMail = function (user, absence) {
    return new Promise(function (resolve, reject) {

        Absence
            .findById(absence)
            .exec(function (err, absence) {
                if (err) reject(err);

                moment.locale('da');
                var requestDateAsDateFormat = new Date(absence.requestDate);
                var formattedRequestDate = moment.utc(requestDateAsDateFormat).format('DD-MM-YYYY HH:mm:ss');
                var startDateAsDateFormat = new Date(absence.startDate);
                var formattedStartDate = moment.utc(startDateAsDateFormat).format('DD-MM-YYYY');
                var endDateAsDateFormat = new Date(absence.endDate);
                var formattedEndDate = moment.utc(endDateAsDateFormat).format('DD-MM-YYYY');

                var formattedStatus = "";
                if (absence.status == "Pending") {
                    formattedStatus = "Afventer behandling";
                } else if (absence.status == "Declined") {
                    formattedStatus = "Afvist";
                } else if (absence.status == "Approved") {
                    formattedStatus = "Godkendt";
                } else {
                    formattedStatus = "Ukendt";
                }

                var privatKommentar;

                if (absence.privateComment) {
                    privatKommentar = "ja";
                } else {
                    privatKommentar = "nej"
                }

                User
                    .findById(user)
                    .exec(function (err, user) {
                        if (err) reject(err);

                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'evkalendersystem@gmail.com',
                                pass: 'r8Dmc#9Q'
                            }
                        });

                        var mailOptions = {
                            from: '"EV-kalender" <evkalendersystem@gmail.com>',
                            to: user.email,
                            subject: "EV-kalender - Dit ferieønske er opdateret",
                            text: 'Brugernavn: ' + user.username + ', Status: ' + formattedStatus + ', Forespørgselstidspunkt: ' + formattedRequestDate + ', Varighed: ' + formattedStartDate + ' - ' + formattedEndDate + ', Kommentar:' + absence.comment + ', Privat:' + absence.privateComment,
                            html: '<table border="1"><tr><th align="left">Brugernavn</th><td>' + user.username + '</td></tr><tr><th align="left">Status</th><td>' + formattedStatus + '</td></tr><tr><th align="left">Forespørgselstidspunkt</th><td>' + formattedRequestDate + '</td></tr><tr><th align="left">Varighed</th><td>' + formattedStartDate + ' - ' + formattedEndDate + '</td></tr><tr><th align="left">Kommentar</th><td>' + absence.comment + '</td></tr><tr><th align="left">Privat</th><td>' + privatKommentar + '</td></tr></tr></table>'
                        };

                        transporter.sendMail(mailOptions, function (error) {
                            if (error) {
                                return error;
                            } else {
                                return 'Message sent';
                            }
                        });
                    })
            })
    })
};

var absenceDeletedMail = function (mailTo, username, startDate, endDate, comment, privateComment) {
    moment.locale('da');
    var deleteDate = new Date();
    var formattedDeleteDate = moment.utc(deleteDate.setTime(deleteDate.getTime() - new Date(deleteDate).getTimezoneOffset() * 60 * 1000)).format('DD-MM-YYYY HH:mm:ss');
    var startDateAsDateFormat = new Date(startDate);
    var formattedStartDate = moment.utc(startDateAsDateFormat).format('DD-MM-YYYY');
    var endDateAsDateFormat = new Date(endDate);
    var formattedEndDate = moment.utc(endDateAsDateFormat).format('DD-MM-YYYY');

    var privatKommentar;

    if (privateComment) {
        privatKommentar = "ja";
    } else {
        privatKommentar = "nej"
    }

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'evkalendersystem@gmail.com',
            pass: 'r8Dmc#9Q'
        }
    });

    var mailOptions = {
        from: '"EV-kalender" <evkalendersystem@gmail.com>',
        to: mailTo,
        subject: "EV-kalender - dit ferieønske er slettet",
        text: 'Brugernavn: ' + username + ', Slettet: ' + formattedDeleteDate + ', Varighed: ' + formattedStartDate + ' - ' + formattedEndDate + ', Kommentar:' + comment + ', Privat:' + privateComment,
        html: '<table border="1"><tr><th align="left">Brugernavn</th><td>' + username + '</td></tr><tr><th align="left">Slettet</th><td>' + formattedDeleteDate + '</td></tr><tr><th align="left">Varighed</th><td>' + formattedStartDate + ' - ' + formattedEndDate + '</td></tr><tr><th align="left">Kommentar</th><td>' + comment + '</td></tr></tr><tr><th align="left">Privat</th><td>' + privatKommentar + '</td></tr></tr></table>'
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return error;
        } else {
            return 'Message sent';
        }
    });
};


exports.getAbsences = function () {
    return new Promise(function (resolve, reject) {
        Absence.find(function (err, absences) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(absences);
                }
            }
        );
    });
};

exports.getAbsences = function (user) {
    return new Promise(function (resolve, reject) {
        Absence.find({'user': user}, function (err, absences) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(absences);
                }
            }
        );
    });
};


exports.getAbsence = function (id) {
    return new Promise(function (resolve, reject) {
        Absence.findOne({'_id': id}, function (err, absence) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(absence);
                }
            }
        );
    });
};

exports.updateAbsenceStatus = function (id, status) {
    return new Promise(function (resolve, reject) {
        Absence.findOne({'_id': id}, function (err, absence) {
                if (err) {
                    reject(err);
                }
                else {
                    if (status == 1) {
                        absence.status = "Approved";
                    } else if (status == 2) {
                        absence.status = "Declined";
                    } else {
                        absence.status = "Pending";
                    }
                    var userId = absence.user;
                    User.findOne({'_id': userId}, function (err, user) {
                        if (err) {
                            reject(err);
                        } else {
                            absence.save(function (err) {
                                if (err) {
                                    reject(err);
                                } else {
                                    sendMail(user.email, user.username, absence.status, absence.requestDate, absence.startDate, absence.endDate, absence.comment);
                                    resolve(absence);
                                }
                            });
                        }
                    })
                }
            }
        );
    });
};

exports.getUsersByApprover = function (approver) {
    return new Promise(function (resolve, reject) {
        User.find({'approver': approver}, function (err, users) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(users);
                }
            }
        );
    });
};

exports.getAbsences2 = function (from, to) {

    return new Promise(function (resolve, reject) {

        start = new Date(0);
        start.setSeconds(from);

        end = new Date(0);
        end.setSeconds(to);

        Absence.find({"startDate": {"$lte": end}, "endDate": {"$gt": start}, "status": {$ne: "Declined"}}, function (err, absences) {
            if (err) {
                reject('den her skulle reject');
            }
            else {
                resolve(absences);
            }
        })
    })
};

exports.checkAbsenceForValidity = function (user, absence, startDate, endDate) {
    var validAbsence = true;
    var i = 0;

    return new Promise(function (resolve, reject) {
        Absence
            .find({user: user})
            .exec(function (err, absences) {
                if (err) reject(err);
                while (validAbsence && i < absences.length) {
                    var absenceStatus = absences[i].status;
                    var absenceStartDate = Date.parse(absences[i].startDate);
                    var absenceEndDate = Date.parse(absences[i].endDate);
                    var inputStartDate = Date.parse(startDate);
                    var inputEndDate = Date.parse(endDate);

                    if (absences[i]._id != absence) {
                        if (absenceStartDate <= inputStartDate && inputStartDate <= absenceEndDate) {
                            if (absenceStatus != "Declined") {
                                validAbsence = false;
                            }
                        } else if (absenceStartDate <= inputEndDate && inputEndDate <= absenceEndDate) {
                            if (absenceStatus != "Declined") {
                                validAbsence = false;
                            }
                        } else if (inputStartDate <= absenceStartDate && absenceStartDate <= inputEndDate) {
                            if (absenceStatus != "Declined") {
                                validAbsence = false;
                            }
                        } else if (inputStartDate <= absenceEndDate && absenceEndDate <= inputEndDate) {
                            if (absenceStatus != "Declined") {
                                validAbsence = false;
                            }
                        }
                    }
                    i++;
                }
                if (validAbsence) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
    })
};

exports.createAbsence = function (user, startDate, endDate, status, comment, privateComment, requestDate) {
    return new Promise(function (resolve, reject) {
        User
            .findById(user)
            .exec(function (err, user) {
                if (err) reject(err);
                var absence = new Absence({
                    user: user,
                    startDate: startDate,
                    endDate: endDate,
                    status: status,
                    comment: comment,
                    privateComment: privateComment,
                    requestDate: requestDate
                });

                absence.save(function (err, absence) {
                    if (err) reject(err);
                    else {
                        user.absences.push(absence._id);
                        user.save();
                        // Send kvitteringsmail til brugeren
                        sendMail(user.email, user.username, status, requestDate, startDate, endDate, comment);
                        resolve(absence);
                    }
                });
            })
    })
};

exports.updateAbsenceApprover = function (absence, startDate, endDate, comment, privateComment, requestDate) {
    return new Promise(function (resolve, reject) {
        Absence.findOneAndUpdate({_id: absence}, {
            startDate: startDate,
            endDate: endDate,
            comment: comment,
            privateComment: privateComment,
            requestDate: requestDate
        }, function (err, absence) {
            if (err) reject(err);
            absence.save();
            User.findById(absence.user).exec(function (err, user) {
                if (err) reject(err);
                resolve("Absence updated!");
            });
        })
    })
};

exports.updateAbsenceUser = function (absence, comment, privateComment, requestDate) {
    return new Promise(function (resolve, reject) {
        Absence.findOneAndUpdate({_id: absence}, {
            comment: comment,
            privateComment: privateComment,
            requestDate: requestDate
        }, function (err) {
            if (err) reject(err);
            else resolve("Absence updated!");
        })
    });
};

// exports.approveAbsence = function (absence) {
//     return new Promise(function (resolve, reject) {
//         Absence.findOneAndUpdate({_id: absence}, {status: "Approved"}, function (err) {
//             if (err) reject(err);
//             resolve("Absence approved!");
//         });
//     })
// };
//
// exports.declineAbsence = function (absence) {
//     return new Promise(function (resolve, reject) {
//         Absence.findOneAndUpdate({_id: absence}, {status: "Declined"}, function (err, absence) {
//             if (err) reject(err);
//             User.findById(absence.user).exec(function (err, user) {
//                 if (err) reject(err);
//                 sendMail(user.email, user.username, absence.status, absence.requestDate, absence.startDate, absence.endDate, absence.comment);
//                 resolve("Absence declined!");
//
//             });
//         })
//     });
// };

exports.deleteAbsence = function (user, absence) {
    return Promise.all([
        new Promise(function (resolve, reject) {
            User
                .findById(user)
                .exec(function (err, user) {
                    if (err) reject(err);
                    user.absences.splice(user.absences.indexOf(absence), 1);
                    user.save();
                    resolve("Absence deleted!");

                })
        }),
        new Promise(function (resolve, reject) {
            Absence
                .findOneAndRemove({_id: absence}, function (err, absence) {
                    if (err) reject(err);
                    User.findById(absence.user).exec(function (err, user) {
                        if (err) reject(err);
                        absenceDeletedMail(user.email, user.username, absence.startDate, absence.endDate, absence.comment, absence.privateComment);
                        resolve("Absence deleted!");
                    });
                });
        })])
};


/**
 * Returns a promise that resolves with a User object
 * @returns {Promise}
 */
exports.registerUser = function (username, password, firstName, surName, email, approver, rights) {
    return new Promise(function (resolve, reject) {
        var user = new User({
            username: username.toLowerCase(),
            password: password,
            firstName: firstName,
            surName: surName,
            email: email,
            approver: approver,
            rights: rights
        });
        user.save(function (err) {
            if (err) {
                reject(err);
            } else {
                resolve(user);
            }
        });
    });
};

/**
 * Returns a promise that resolves with a User object
 * @returns {Promise}
 */
exports.setSelfApprove = function (userID) {
    return new Promise(function (resolve, reject) {
        User.findOneAndUpdate({'_id': userID}, {$set: {approver: userID}}, function (err, user) {
            if (err) {
                reject(err);
            } else {
                resolve(user);
            }
        });
    });
};

/**
 * Returnerer true hvis brugeren blev fjernet, ellers false
 * @returns {Promise}
 */
exports.deleteUser = function (id) {
    return new Promise(function (resolve, reject) {
        User.findOneAndRemove({'_id': id}, function (err, user) {
            if (err) {
                reject(err);
            } else {
                if (user != null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            }
        });
    });
};

/**
 * Returns a promise that resolves with an array of all users.
 * @returns {Promise}
 */
exports.getUsers = function () {
    return new Promise(function (resolve, reject) {
        User.find(function (err, users) {
            if (err) {
                reject(err);
            } else {
                resolve(users);
            }
        });
    });
};

/**
 * Returns a promise that resolves with an array of all users.
 * @returns {Promise}
 */
exports.getUser = function (id) {
    return new Promise(function (resolve, reject) {
        User.findOne({'_id': id}, function (err, user) {
            if (err) {
                reject(err);
            } else {
                resolve(user);
            }
        });
    });
};

/**
 * Returns a promise that resolves with an array of all admins (rights property = 1).
 * @returns {Promise}
 */
exports.getAdmins = function () {
    return new Promise(function (resolve, reject) {
        User.find({'rights': '1'}, function (err, admins) {
            if (err) {
                reject(err);
            } else {
                resolve(admins);
            }
        });
    });
};

/**
 * Henter det angivne brugernavn, hvis brugeren findes i MongoDB, tjekkes passwordet med comparePassword.
 * Hvis brugeren findes returneres objektet, ellers returneres beskeden "User not found!".
 *
 */
exports.userLogin = function (username, password) {
    return new Promise(function (resolve, reject) {
        if (typeof username !== 'string') {
            reject("Username must be a string!");
        } else {
            // Hent user
            User.findOne({username: username.toLowerCase()}, function (err, user) {
                if (err) {
                    reject(err);
                } else {
                    // User found, not null
                    if (user != null) {
                        // Compare input password med det gemte i MongoDB
                        user.comparePassword(password, function (err, isMatch) {
                            if (err) {
                                reject(err);
                            } else if (isMatch) {
                                // Korrekt login isMatch = true
                                resolve(user);
                            } else {
                                // Reject 1 = Forkert adgangskode
                                reject(1);
                            }
                        });
                    } else {
                        // Reject 2 = Brugeren blev ikke fundet!
                        reject(2);
                    }
                }
            });
        }
    });
};